// Core Data Types
type [ Bool ] where 
    :> false => -- Bool ;
    :> true  => -- Bool ;
;

type [ a Optional ] where
    :> none =>   -- [ a Optional ] ;
    :> just => a -- [ a Optional ] ;
;

type [ a List ] where
    :> cons => a [ a List ] -- [ a List ] ;
    :> nil  =>              -- [ a List ] ;
;

type [ a b Either ] where
    :> this => a -- [ a b Either ] ;
    :> that => b -- [ a b Either ] ;
;

// Duplicators
:> dup => x -- x x ;
:: dup
    (prim-dup) ;

:> dup2 => x y -- x y x y ;
:: dup2
    (prim-dup2) ;

:> dup3 => x -- x x ;
:: dup3
    (prim-dup3) ;

:> dup4 => x -- x x ;
:: dup4
    (prim-dup4) ;

// Droppers
:> drop => x -- ;
:: drop
    (prim-drop) ;

:> drop2 => x y -- ;
:: drop2
    (prim-drop2) ;

:> drop3 => x -- ;
:: drop3
    (prim-drop3) ;

:> drop4 => x -- ;
:: drop4 
    (prim-drop4) ;

// Nippers
:> nip => x y -- y ;
:: nip (prim-nip) ;

:> nip2 => x y z -- z ;
:: nip2 (prim-nip2)

:> nip3 => x y z w -- w ;
:: nip3 (prim-nip3)

:> nip4 => x y z w v -- v ;
:: nip4 (prim-nip4)

// Swappers
:> swap => x y -- y x ;
:: swap
    (prim-swap) ;

// Dippers
// Most of these definitions are taken from
// Factor
:> dip => $p x ( $p -- $q ) -- $q x ;
:: dip 
    swap [ apply ] dip ;

:> dip2 => $p x y ( $p -- $q ) -- $q x y ;
:: 2dip 
    swap [ dip ] dip ; 

:> dip3 => $p x y z ( $p -- $q ) -- $q x y z ;
:: dip3 
    swap [ dip2 ] dip ;

:> dip4 => $p x y z w ( $p -- $q ) -- $q x y z w ;
:: dip4 
    swap [ dip3 ] dip ;

:> apply => $p ( $p -- $q ) -- $q ;
:: apply
    (prim-apply) ;

:> quote => x -- [ -- x ] ;
:: quote
    (prim-quote) ;

:> compose => ( $p -- $q ) ( $q -- $r ) -- ( $p -- $r ) ;
:: compose
    (prim-compose) ;

:> prepose => ( $q -- $p ) ( $r -- $q ) -- ( $r -- $p ) ;
:: prepose
    swap compose ;

// Currying
:> curry => x ( $q x -- $p ) -- ( $q -- $p ) ;
:: curry 
    (prim-curry) ;

:> curry2 => x y ( $p x y -- $q ) -- ( $p -- $q ) ;
:: curry2
    curry curry ; 

:> curry3 => x y z ( $p x y z -- $q ) -- ( $p -- $q ) ;
:: curry3
    curry curry curry ;

/// Cleavers
// Single
:> bi => x ( x -- a ) ( x -- b ) -- a b ;
:: bi
    [ keep ] dip apply ;

:> tri => x ( x -- a ) ( x -- b ) ( x -- c ) -- a b c ;
:: tri
    [ [ keep ] dip keep ] dip apply ;

// Double
:> bi2 => x y [ x y -- a ] [ x y -- b ] -- a b ;
:: bi2
    [ keep2 ] dip apply ;

:> tri2 => x y [ x y -- a ] [ x y -- b ] [ x y -- c ] -- a b c ;
:: tri2
    [ [ keep2 ] dip keep2 ] dip apply ;

// Triple
:> bi3 => x y z [ x y z -- a ] [ x y -- b ] -- a b ;
:: bi3
    [ keep3 ] dip apply ;

:> tri3 => x y z [ x y z -- a ] [ x y z -- b ] [ x y z -- c ] -- a b c ;
:: tri3
    [ [ keep3 ] dip 3keep ] dip apply ;

/// Spreads
// Single
:> bi* => ( x y [ x -- x' ] [ y -- y' ] -- x' y' )
:: bi* 
    [ dip ] dip apply ;

:> tri* => x y z ( x -- x' ) ( y -- y' ) ( z -- z' ) -- x' y' z'
:: tri*
    [ [ dip2 ] dip dip ] dip apply ;

/// Applies
:> bi@ => x x ( x -- y ) -- y y 
:: bi@
    dup bi* ;

:> tri@ => x x x ( x -- y ) -- y y y
:: tri@
    dup tri* ;

/// Boolean Logic
:> not => Bool -- Bool
:: not then { false } else { true } ;

:> or => Bool -- Bool
:: or then { true } else { pass } ;

:> and => Bool -- Bool
:: and then { pass } else { false } ;
