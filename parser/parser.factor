! Copyright (C) 2022 Your name.
! See http://factorcode.org/license.txt for BSD license.
USING: kernel math.parser multiline peg peg.ebnf sequences
strings unicode vectors ;
IN: wyrm.parser

TUPLE: data-type
    { name string } ;
: <data-type> ( name -- data-type )
    data-type boa ;

TUPLE: generic-type 
    { name string } ;
: <generic-type> ( name -- generic-type )
    generic-type boa ;

: data-or-generic-type ( name -- data-type/generic-type )
    dup first letter? [ <generic-type> ] [ <data-type> ] if ;

TUPLE: parameterized-type
    { types vector } ;

: <parameterized-type> ( vector -- parameterized-type )
    parameterized-type boa ;

TUPLE: tuple-type
    { types vector } ;
: <tuple-type> ( vector -- tuple-type )
    tuple-type boa ;

TUPLE: quote-type 
    { in   vector }
    { out  vector } ;

: <quote-type> ( vector vector -- quote-type )
    quote-type boa ;

: >quote ( vector -- quote-type )
    [ first ] [ second ] bi <quote-type> ;

TUPLE: row-type 
    { name string } ;
    
: <row-type> ( string -- row-type )
    row-type boa ;

: >row-type ( vector -- row-type )
    "" join <row-type> ;

EBNF: declare-word [=[
    space   = (" " | "\r" | "\t" | "\n")
    s       = (space+)~
    delim   = ("=>" | "--" | "[" | "]" | "{" | "}" | ";" | "(" | ")" | "#" | space)
    

    declare   = ":>"~
    arrow     = "=>"~
    dashes    = "--"~
    end       = ";"~
    l-bracket = "["~
    r-bracket = "]"~
    l-brace   = "{"~
    r-brace   = "}"~
    l-paren   = "("~
    r-paren   = ")"~
    l-tuple   = "#("~

    stack-eff   = s (type s)* dashes s (type s)* 
    name        = (!(delim) .)+ => [[ >string ]]
    type        = (row | type-name | compound  | quote | tuple)

    type-name   = name                            => [[ data-or-generic-type ]]
    compound    = l-bracket s (type s)+ r-bracket => [[ <parameterized-type> ]]
    tuple       = l-tuple s (type s)+ r-paren     => [[ <tuple-type> ]]
    quote       = l-paren stack-eff r-paren       => [[ >quote ]]
    row         = "." "." name                    => [[ >row-type ]]
    
    perm-opts   = "+IO"
    perms       = l-brace s (perm-opts)* s r-brace s
    
    rule = (s?)~ declare s name s arrow stack-eff perms? end
]=]


EBNF: define-word [=[
    space  = (" " | "\r" | "\t" | "\n")
    s      = space+~
    
    literal = (string | char | double | integer | quote | tuple | list)

    string-contents = (!("\"").)* => [[ >string ]]

    string  = "\"" string-contents  "\""
    char    = "'"~  (!("'").) "'"~ 
    num     = [0-9]+                             => [[ >string ]]
    double  = (num "." num | num "." | "." num)  => [[ concat string>number ]]
    integer = [0-9]+                             => [[ >string string>number ]]

    l-bracket = "["~
    r-bracket = "]"~
    quote     = l-bracket s (literal s)* r-bracket

    l-tuple = "#("~
    r-tuple = ")"~
    tuple   = l-tuple s (literal s)* r-tuple

    l-brace = "{"~
    r-brace = "}"~
    list    = l-brace s (literal s)* r-tuple

    define = "::"~
    word   = (!(space | end) .)+    => [[ >string ]]
    body   = ((literal | word) s)*
    end    = ";"~

    rules = (s?)~ define s word s body end
]=]

EBNF: define-data [=[
    rules = " "
]=]

EBNF: define-trait [=[
    rules = " "
]=]

