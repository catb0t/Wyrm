! Copyright (C) 2022 Capital Ex.
! See http://factorcode.org/license.txt for BSD license.

USING: accessors assocs combinators kernel math multiline
namespaces peg peg.ebnf prettyprint sequences sequences.parser
splitting strings typed unicode variants vectors ;
IN: wyrm


! Look up dictionaries for the 3 main types of names in Wyrm:
!   - Words
!   - Data Types
!   - Traits
SYMBOL: @word-dictionary
SYMBOL: @data-dictionary
SYMBOL: @trait-dictionary

! Variable to hold previous word that has been parsed.
SYMBOL: @last-word


: initalize-dictionaries ( -- )
    @word-dictionary 
    @data-dictionary 
    @trait-dictionary 
    [ H{ } clone swap set ] tri@ ;


: compile ( string -- )
    drop ;